package ru.frosteye.ovsa.commons.router

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment

class ContainerActivity : AppCompatActivity() {

    private var hasBackStack = false
    private var themeRes = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        themeRes = intent.getIntExtra(RouterKeys.THEME_RES, -1)
        if (themeRes != -1) {
            setTheme(themeRes)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container)

        handleRequest()
    }

    private fun handleRequest() {
        hasBackStack = intent.getBooleanExtra(RouterKeys.HAS_BACK_STACK, false)
        try {
            val fragmentName = intent.getStringExtra(RouterKeys.TARGET_FRAGMENT_CLASS)
                ?: throw IllegalStateException("no fragment for activity")
            val clazz = Class.forName(fragmentName)
            val fragment = clazz.newInstance() as Fragment
            fragment.arguments = intent.getParcelableExtra(RouterKeys.LAUNCH_PAYLOAD)
            setContent(fragment)
        } catch (e: Exception) {
            e.printStackTrace()
            finish()
        }
    }

    override fun invalidateOptionsMenu() {
        super.invalidateOptionsMenu()
        supportActionBar?.setDisplayHomeAsUpEnabled(hasBackStack)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setContent(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, fragment)
            .commit()
    }

    companion object {

        fun start(
            context: Context,
            fragment: Class<out Fragment>,
            params: StandaloneFragmentParams
        ) {
            if (context is Activity) {
                /*val options = ActivityOptionsCompat.makeSceneTransitionAnimation(context)*/
                context.startActivity(
                    createIntent(
                        context,
                        fragment,
                        params
                    ),
                    /*options.toBundle()*/
                )
            } else {
                context.startActivity(
                    createIntent(
                        context,
                        fragment,
                        params
                    )
                )
            }

        }

        fun createIntent(
            context: Context,
            fragment: Class<out Fragment>,
            params: StandaloneFragmentParams
        ): Intent {
            return Intent(context, ContainerActivity::class.java).apply {
                val targetFragmentClass = fragment.name
                if (params.startInNewTask) {
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                }
                putExtra(RouterKeys.HAS_BACK_STACK, params.hasBackStack)
                putExtra(RouterKeys.TARGET_FRAGMENT_CLASS, targetFragmentClass)
                putExtra(RouterKeys.THEME_RES, params.themeRes)
                params.arguments?.let {
                    putExtra(RouterKeys.LAUNCH_PAYLOAD, it)
                }
            }
        }
    }


}