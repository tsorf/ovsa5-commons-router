package ru.frosteye.ovsa.commons.router

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import io.reactivex.rxjava3.subjects.MaybeSubject
import ru.frosteye.ovsa.commons.extensions.safeCast
import ru.frosteye.ovsa.commons.router.launch.launcher.Launcher
import ru.frosteye.ovsa.commons.router.launch.activity.ActivityLauncher
import ru.frosteye.ovsa.commons.router.launch.fragment.BottomSheetFragmentLauncher
import ru.frosteye.ovsa.commons.router.launch.fragment.ContainedFragmentLauncher
import ru.frosteye.ovsa.commons.router.stack.FragmentStack
import ru.frosteye.ovsa.commons.router.stack.FragmentStackHost
import ru.frosteye.ovsa.mvvm.router.MvvmRouter
import ru.frosteye.ovsa.mvvm.router.RouterResult
import java.util.*

open class DefaultActivityRouter(
    protected val activity: AppCompatActivity
) : MvvmRouter {

    private val fragmentManager = activity.supportFragmentManager

    protected fun <I : Any, O> launcherForFragment(
        clazz: Class<out Fragment>,
        launch: Launcher.FragmentLaunch
    ): Launcher<I, O> {
        when (launch) {
            is Launcher.FragmentLaunch.Standalone -> {
                return launcherForActivityInner(
                    ContainerActivity.createIntent(
                        activity, clazz, StandaloneFragmentParams(
                            launch.hasBackStack,
                            null,
                            launch.startInNewTask,
                            launch.themeRes
                        )
                    )
                )
            }
            is Launcher.FragmentLaunch.InContainer -> {
                val publisher = MaybeSubject.create<O>()
                return ContainedFragmentLauncher(
                    UUID.randomUUID().toString(),
                    fragmentManager,
                    launch.containerLayoutId,
                    clazz
                )
            }
            is Launcher.FragmentLaunch.InBottomSheet -> {
                val publisher = MaybeSubject.create<O>()
                return BottomSheetFragmentLauncher(
                    UUID.randomUUID().toString(),
                    fragmentManager,
                    clazz
                )
            }
        }
    }

    protected fun <I : Any, O> launcherForActivity(clazz: Class<out Activity>): Launcher<I, O> {
        val intent = Intent(activity, clazz)
        return launcherForActivityInner(intent)
    }

    private fun <I : Any, O> launcherForActivityInner(intent: Intent): Launcher<I, O> {
        return ActivityLauncher(activity, intent)
    }


    protected fun navigateToActivity(
        clazz: Class<out Activity>,
        params: Bundle? = null
    ) {
        activity.startActivity(Intent(activity, clazz).apply {
            params?.let {
                putExtras(it)
            }
        })
    }

    /*protected fun navigateToFragment(
        clazz: Class<out Fragment>,
        launch: Launcher.FragmentLaunch,
        argument: Parcelable? = null
    ) {
        when (launch) {
            is Launcher.FragmentLaunch.Standalone -> {
                ContainerActivity.start(
                    activity, clazz, StandaloneFragmentParams(
                        launch.hasBackStack,
                        Bundle().apply {
                            if (argument != null) {
                                putParcelable(argument::class.java.name, argument)
                            }
                        },
                        launch.startInNewTask,
                        launch.themeRes
                    )
                )
            }
            is Launcher.FragmentLaunch.InContainer -> {
                ContainedFragmentLauncher<Parcelable, Void>(
                    null,
                    fragmentManager,
                    launch.containerLayoutId,
                    clazz
                ).launch(argument)
            }
            is Launcher.FragmentLaunch.InBottomSheet -> {
                BottomSheetFragmentLauncher<Parcelable, Void>(
                    null,
                    fragmentManager,
                    clazz
                ).launch(argument)
            }
        }
    }*/

    override fun navigateBack(result: RouterResult?) {
        when {
            fragmentManager.backStackEntryCount > 0 -> {
                if (result != null && result is OvsaRouterResult) {
                    when (result) {
                        is OvsaRouterResult.Success -> {
                            val payload = result.payload<Parcelable>()
                            if (payload != null) {
                                val bundle = Bundle().apply {
                                    putParcelable(RouterKeys.RESULT_PAYLOAD, payload)
                                }
                                fragmentManager.setFragmentResult(result.requestCode, bundle)
                            }
                        }
                        is OvsaRouterResult.Failure -> {
                            //TODO
                        }

                        is OvsaRouterResult.Canceled -> {
                            //TODO
                        }
                    }
                }
                fragmentManager.popBackStack()
            }
            else -> {
                if (result != null && result is OvsaRouterResult) {
                    when (result) {
                        is OvsaRouterResult.Success -> {
                            val payload = result.payload<Parcelable>()
                            if (payload != null) {
                                activity.setResult(
                                    Activity.RESULT_OK,
                                    Intent().putExtra(RouterKeys.RESULT_PAYLOAD, payload)
                                )
                            } else {
                                activity.setResult(Activity.RESULT_OK)
                            }
                        }
                        is OvsaRouterResult.Failure -> {
                            activity.setResult(
                                RouterKeys.RESULT_FAILURE,
                                Intent().putExtra(RouterKeys.KEY_THROWABLE, result.error())
                            )
                        }

                        is OvsaRouterResult.Canceled -> {
                            //TODO
                        }
                    }


                }
                activity.finish()
            }
        }
    }

    protected fun requireFragmentStack(): FragmentStack {
        val stack = activity.safeCast<FragmentStackHost>()?.fragmentStack
        check(stack != null) {
            "$activity is not FragmentStackHost"
        }
        return stack
    }
}


