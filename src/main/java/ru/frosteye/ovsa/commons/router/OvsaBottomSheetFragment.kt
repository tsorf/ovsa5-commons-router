package ru.frosteye.ovsa.commons.router

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class OvsaBottomSheetFragment(
    private val actualFragment: Fragment?
) : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_ovsa_bottom_sheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val fragment = actualFragment
        if (fragment == null) {
            dismiss()
            return
        }
        childFragmentManager.beginTransaction()
            .replace(R.id.bottomSheetContainer, fragment)
            .commit()
    }
}