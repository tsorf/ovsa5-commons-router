package ru.frosteye.ovsa.commons.router

import android.os.Parcelable
import ru.frosteye.ovsa.mvvm.router.RouterResult

sealed class OvsaRouterResult(
    override val type: RouterResult.Type,
    override val requestCode: String
) : RouterResult {

    class Success(
        private val rawPayload: Parcelable?,
        requestCode: String,
    ) : OvsaRouterResult(RouterResult.Type.SUCCESS, requestCode), RouterResult {

        fun <T> payload(): T? {
            if (rawPayload == null) {
                return null
            }
            return rawPayload as? T ?: throw ClassCastException()
        }
    }

    class Failure(
        private val throwable: Throwable,
        requestCode: String,
    ) : OvsaRouterResult(RouterResult.Type.FAILURE, requestCode), RouterResult {

        fun error(): Throwable {
            return throwable
        }
    }

    class Canceled(
        requestCode: String
    ) : OvsaRouterResult(RouterResult.Type.CANCELED, requestCode)

    companion object Factory {

        const val ANY_REQUEST = "any_request"
        const val KEY_REQUEST_CODE = "request_code"

        fun success(
            payload: Parcelable? = null,
            requestCode: String = ANY_REQUEST
        ): OvsaRouterResult {
            return Success(payload, requestCode)
        }

        fun canceled(requestCode: String = ANY_REQUEST): OvsaRouterResult {
            return Canceled(requestCode)
        }

        fun failure(throwable: Throwable, requestCode: String = ANY_REQUEST): OvsaRouterResult {
            return Failure(throwable, requestCode)
        }
    }
}
