package ru.frosteye.ovsa.commons.router

internal object RouterKeys {

    const val HAS_BACK_STACK = "has_back_stack"
    const val TARGET_FRAGMENT_CLASS = "target_fragment_clazz"
    const val LAUNCH_PAYLOAD = "launch_arguments"
    const val THEME_RES = "theme_res"


    const val RESULT_PAYLOAD = "RouterResultPayload"
    const val KEY_THROWABLE = "RouterResultThrowable"
    const val RESULT_FAILURE = 2
}