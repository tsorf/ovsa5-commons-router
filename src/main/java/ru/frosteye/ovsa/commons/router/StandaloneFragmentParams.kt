package ru.frosteye.ovsa.commons.router

import android.os.Bundle
import android.os.Parcelable
import androidx.annotation.StyleRes

data class StandaloneFragmentParams(
    val hasBackStack: Boolean = true,
    val arguments: Bundle? = null,
    val startInNewTask: Boolean = false,
    @StyleRes val themeRes: Int? = null
)

/*
fun StandaloneFragmentParams(
    argument: Parcelable,
    hasBackStack: Boolean = true,
    startInNewTask: Boolean = false,
    @StyleRes themeRes: Int? = null
): StandaloneFragmentParams {
    return StandaloneFragmentParams(
        hasBackStack = hasBackStack,
        arguments = Bundle().apply {
            putParcelable(argument::class.java.name, argument)
        },
        startInNewTask = startInNewTask,
        themeRes = themeRes,
    )
}*/
