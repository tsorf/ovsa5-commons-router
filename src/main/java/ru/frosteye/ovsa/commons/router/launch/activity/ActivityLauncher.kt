package ru.frosteye.ovsa.commons.router.launch.activity

import android.content.Intent
import androidx.activity.result.ActivityResultLauncher
import androidx.appcompat.app.AppCompatActivity
import ru.frosteye.ovsa.commons.router.launch.launcher.Launcher
import ru.frosteye.ovsa.commons.router.launch.launcher.Destination
import ru.frosteye.ovsa.commons.router.launch.launcher.DestinationMaybeImpl
import io.reactivex.rxjava3.subjects.MaybeSubject

class ActivityLauncher<I : Any, O>(
    private val activity: AppCompatActivity,
    private val intent: Intent,
) : Launcher<I, O> {

    private val innerLauncher: ActivityResultLauncher<I>
    private var currentPublisher: MaybeSubject<O>? = null

    init {
        innerLauncher = activity.registerForActivityResult(OvsaActivityResult<I, O>(intent)) {
            if (it != null) {
                onSuccess(it)
            } else {
                onComplete()
            }
        }
    }

    override fun launch(param: I): Destination<O> {
        currentPublisher?.onComplete()
        val newPublisher = MaybeSubject.create<O>()
        currentPublisher = newPublisher
        /*val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity)
        innerLauncher.launch(param, options)*/
        innerLauncher.launch(param)

        return DestinationMaybeImpl(newPublisher)
    }

    private fun onSuccess(result: O) {
        currentPublisher?.onSuccess(result)
    }

    private fun onComplete() {
        currentPublisher?.onComplete()
    }
}