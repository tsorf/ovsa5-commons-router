package ru.frosteye.ovsa.commons.router.launch.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.activity.result.contract.ActivityResultContract
import ru.frosteye.ovsa.commons.router.RouterKeys

class OvsaActivityResult<I : Any, O>(
    private val intent: Intent
) : ActivityResultContract<I, O?>() {


    override fun createIntent(context: Context, input: I): Intent {
        val newIntent = Intent(intent)
        if (input is Parcelable) {
            val arguments = Bundle().apply {
                putParcelable(input::class.java.name, input)
            }
            newIntent.putExtra(RouterKeys.LAUNCH_PAYLOAD, arguments)
        }
        return newIntent
    }

    override fun parseResult(resultCode: Int, intent: Intent?): O? {
        if (resultCode != Activity.RESULT_OK) {
            return null
        }
        return intent?.getParcelableExtra<Parcelable>(RouterKeys.RESULT_PAYLOAD)?.let {
            return it as? O
        }
    }
}