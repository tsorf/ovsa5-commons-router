package ru.frosteye.ovsa.commons.router.launch.fragment

import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResultListener
import ru.frosteye.ovsa.commons.router.OvsaBottomSheetFragment
import ru.frosteye.ovsa.commons.router.OvsaRouterResult
import ru.frosteye.ovsa.commons.router.RouterKeys
import ru.frosteye.ovsa.commons.router.launch.launcher.Launcher
import ru.frosteye.ovsa.commons.router.launch.launcher.Destination
import ru.frosteye.ovsa.commons.router.launch.launcher.DestinationMaybeImpl
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.subjects.MaybeSubject


class BottomSheetFragmentLauncher<I : Any, O>(
    private val requestCode: String?,
    private val fragmentManager: FragmentManager,
    private val target: Class<out Fragment>
) : Launcher<I, O> {


    override fun launch(param: I): Destination<O> {
        val destination = target.newInstance()
        val arguments = Bundle().apply {
            if (param is Parcelable) {
                putParcelable(param::class.java.name, param)
            }
            putString(OvsaRouterResult.KEY_REQUEST_CODE, requestCode)
        }
        destination.arguments = arguments
        val wrapper = OvsaBottomSheetFragment(destination)
        val transaction = fragmentManager.beginTransaction()
            .addToBackStack(requestCode)
        wrapper.show(transaction, requestCode)
        return if (requestCode != null) {
            val publisher = MaybeSubject.create<O>()
            wrapper.setFragmentResultListener(requestCode) { _, bundle ->
                val result = bundle.getParcelable<Parcelable>(RouterKeys.RESULT_PAYLOAD)
                val castedResult = (result as? O)
                if (castedResult != null) {
                    publisher.onSuccess(castedResult)
                } else {
                    publisher.onComplete()
                }
            }
            DestinationMaybeImpl(publisher)
        } else {
            DestinationMaybeImpl(Maybe.empty())
        }
    }
}