package ru.frosteye.ovsa.commons.router.launch.fragment

import android.os.Bundle
import android.os.Parcelable
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResultListener
import ru.frosteye.ovsa.commons.router.OvsaRouterResult
import ru.frosteye.ovsa.commons.router.RouterKeys
import ru.frosteye.ovsa.commons.router.launch.launcher.Launcher
import ru.frosteye.ovsa.commons.router.launch.launcher.Destination
import ru.frosteye.ovsa.commons.router.launch.launcher.DestinationMaybeImpl
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.subjects.MaybeSubject


class ContainedFragmentLauncher<I : Any, O>(
    private val requestCode: String?,
    private val fragmentManager: FragmentManager,
    @IdRes private val containerLayoutId: Int,
    private val target: Class<out Fragment>
) : Launcher<I, O> {


    override fun launch(param: I): Destination<O> {
        val destination = target.newInstance()
        val arguments = Bundle().apply {
            if (param is Parcelable) {
                putParcelable(param::class.java.name, param)
            }
            putString(OvsaRouterResult.KEY_REQUEST_CODE, requestCode)
        }
        destination.arguments = arguments
        val publisher: Maybe<O>
        if (requestCode != null) {
            val subject = MaybeSubject.create<O>()
            publisher = subject
            destination.setFragmentResultListener(requestCode) { uuid, bundle ->
                val result = bundle.getParcelable<Parcelable>(RouterKeys.RESULT_PAYLOAD)
                val castedResult = (result as? O)
                if (castedResult != null) {
                    subject.onSuccess(castedResult)
                } else {
                    subject.onComplete()
                }
            }
        } else {
            publisher = MaybeSubject.empty()
        }
        fragmentManager.beginTransaction()
            .addToBackStack(requestCode)
            .add(containerLayoutId, destination)
            .commit()
        return DestinationMaybeImpl(publisher)
    }
}