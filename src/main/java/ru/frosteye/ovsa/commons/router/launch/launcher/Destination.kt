package ru.frosteye.ovsa.commons.router.launch.launcher


interface Destination<O> {

    fun subscribe(
        success: (O) -> Unit,
        error: (Throwable) -> Unit,
        cancel: () -> Unit = {},
    ): Destination<O>

    fun unsubscribe()
}

fun <O> Destination<O>.subscribe(success: (O) -> Unit): Destination<O> {
    return subscribe(success, {
        it.printStackTrace()
    })
}