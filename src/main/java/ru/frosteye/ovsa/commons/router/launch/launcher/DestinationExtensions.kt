package ru.frosteye.ovsa.commons.router.launch.launcher

import ru.frosteye.ovsa.commons.rx.container.MaybeContainer
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.subjects.MaybeSubject

fun <O> Destination<O>.asMaybe(): Maybe<O> {
    return if (this is MaybeContainer<*>) {
        getMaybe() as Maybe<O>
    } else {
        val publisher = MaybeSubject.create<O>()
        subscribe({
            publisher.onSuccess(it)
        }, {
            publisher.onError(it)
        }, {
            publisher.onComplete()
        })
        publisher
    }
}