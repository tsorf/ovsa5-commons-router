package ru.frosteye.ovsa.commons.router.launch.launcher

import ru.frosteye.ovsa.commons.rx.container.MaybeContainer
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.disposables.Disposable

internal class DestinationMaybeImpl<T>(
    source: Maybe<T>
) : Destination<T>, MaybeContainer<T> {

    private var disposable: Disposable? = null

    private val _source: Maybe<T> = source

    override fun getMaybe(): Maybe<T> {
        return _source
    }

    override fun subscribe(
        success: (T) -> Unit,
        error: (Throwable) -> Unit,
        cancel: () -> Unit
    ): Destination<T> {
        disposable = _source
            .subscribe({
                success(it)
            }, {
                it.printStackTrace()
                error(handleError(it))
            }, {
                cancel()
            })
        return this
    }

    override fun unsubscribe() {
        disposable?.dispose()
    }

    private fun handleError(t: Throwable): Throwable {
        return t
    }
}