package ru.frosteye.ovsa.commons.router.launch.launcher

import androidx.annotation.IdRes
import androidx.annotation.StyleRes

interface Launcher<I, O> {

    fun launch(param: I): Destination<O>

    sealed class FragmentLaunch {

        data class Standalone(
            val hasBackStack: Boolean = true,
            val startInNewTask: Boolean = false,
            @StyleRes val themeRes: Int? = null
        ) : FragmentLaunch()

        data class InContainer(
            @IdRes val containerLayoutId: Int,
        ) : FragmentLaunch()

        object InBottomSheet : FragmentLaunch()
    }

}