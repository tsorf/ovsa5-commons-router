package ru.frosteye.ovsa.commons.router.stack

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import java.util.*

class DefaultFragmentStack(
    private val container: EmbeddingContainer,
    roots: List<Fragment>,
    startWithPage: Int = 0,
    private val beforeCommitTransaction: (FragmentTransaction) -> Unit = {}
) : FragmentStack {

    private val stacks: List<LinkedList<Fragment>>
    private var currentStackIndex = 0
    private var currentFragment: Fragment? = null
    private val currentStack: Deque<Fragment>
        get() {
            return stacks[currentStackIndex]
        }

    init {
        require(roots.isNotEmpty())
        require(startWithPage >= 0 && startWithPage < roots.size)

        stacks = roots.map {
            LinkedList(listOf(it))
        }
        currentStackIndex = startWithPage
        switchToRootAt(startWithPage)
    }

    override fun switchToRootAt(position: Int) {
        require(position < stacks.size)
        currentStackIndex = position
        val stack = currentStack
        val fragment = stack.last
        requireNotNull(fragment)
        showFragment(fragment)
    }

    override fun goBack(): Boolean {
        val stack = currentStack
        return if (stack.size > 1) {
            val poppedFragment = stack.pollLast()
            requireNotNull(poppedFragment)

            val newTailFragment = stack.peekLast()
            requireNotNull(newTailFragment)

            showFragment(newTailFragment)
            true
        } else {
            false
        }
    }

    override fun addFragment(fragment: Fragment) {
        val stack = currentStack
        stack.addLast(fragment)
        showFragment(fragment)
    }

    private fun showFragment(fragment: Fragment) {
        val transaction = container.fragmentManager.beginTransaction()
        currentFragment?.let {
            transaction.hide(it)
        }
        var shouldRefreshFragment = false
        if (fragment.isAdded) {
            transaction
                .show(fragment)
        } else {
            shouldRefreshFragment = true
            transaction
                .add(container.containerView.id, fragment)
        }
        beforeCommitTransaction(transaction)
        transaction.commitNow()
        currentFragment = fragment
        if (shouldRefreshFragment) {
            fragment.onHiddenChanged(false)
        }
    }
}