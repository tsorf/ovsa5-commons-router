package ru.frosteye.ovsa.commons.router.stack

import android.view.ViewGroup
import androidx.fragment.app.FragmentManager

data class EmbeddingContainer(
    val fragmentManager: FragmentManager,
    val containerView: ViewGroup
)
