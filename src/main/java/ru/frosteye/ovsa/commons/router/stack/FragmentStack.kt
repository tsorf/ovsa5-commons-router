package ru.frosteye.ovsa.commons.router.stack

import androidx.fragment.app.Fragment

interface FragmentStack {

    fun switchToRootAt(position: Int)

    fun goBack(): Boolean

    fun addFragment(fragment: Fragment)
}