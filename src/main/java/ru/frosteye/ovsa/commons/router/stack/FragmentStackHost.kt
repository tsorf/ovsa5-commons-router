package ru.frosteye.ovsa.commons.router.stack

interface FragmentStackHost {

    val fragmentStack: FragmentStack
}