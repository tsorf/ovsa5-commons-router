package ru.frosteye.ovsa.commons.router.support

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import ru.frosteye.ovsa.commons.router.DefaultActivityRouter

fun Fragment.defaultRouter(): Lazy<DefaultActivityRouter> {
    return lazy {
        val activity = requireActivity()
        if (activity !is AppCompatActivity) {
            throw IllegalStateException("$activity is not an AppCompatActivity")
        }
        DefaultActivityRouter(activity)
    }
}

fun FragmentActivity.defaultRouter(): Lazy<DefaultActivityRouter> {
    if (this !is AppCompatActivity) {
        throw IllegalStateException("$this is not an AppCompatActivity")
    }
    return lazy { DefaultActivityRouter(this) }
}