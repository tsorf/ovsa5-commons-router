package ru.frosteye.ovsa.commons.router.viewstack

import java.util.*

internal class DefaultStateStack<T : StackEntry> : StateStack<T> {

    private val stack: Deque<T> = LinkedList()

    override fun put(entry: T, clearTop: Boolean) {
        if (clearTop) {
            val last = stack.peekLast()
            if (last != null && last.identifier == entry.identifier) {
                stack.pollLast()
            }
        }
        stack.addLast(entry)
    }

    override fun pop(): T? {
        if (stack.isEmpty()) {
            return null
        }
        stack.pollLast()
        return stack.peekLast()
    }
}

fun <T : StackEntry> defaultStateStack(): StateStack<T> {
    return DefaultStateStack()
}