package ru.frosteye.ovsa.commons.router.viewstack

interface StackEntry {

    val identifier: Long
}