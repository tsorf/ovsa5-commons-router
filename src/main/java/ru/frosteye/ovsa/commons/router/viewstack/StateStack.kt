package ru.frosteye.ovsa.commons.router.viewstack

interface StateStack<T : StackEntry> {

    fun put(entry: T, clearTop: Boolean = true)

    fun pop(): T?
}